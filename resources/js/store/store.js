import Vuex from 'vuex'
import Vue from 'vue'
import axios from 'axios'

Vue.use(Vuex)
Vue.prototype.$axios = axios

export default new Vuex.Store({
    strict: true,
    state: {
        totalProductCount: 10,
        allProducts: [],
        Cart: [],
        loading: false
    },

    getters: {
    },

    mutations: {

        SET_PRODUCTS(state, allProducts) {
            state.allProducts = allProducts
        },

        addtoCart(state, product) {
            const array = state.Cart
            array.push(product)
            console.log(array)
        },

        replaceProductByID(state, product) {
            const oldProduct = state.allProducts.find(({ id }) => product.id == id)
            const oldProductIndex = state.allProducts.indexOf(oldProduct)
            Vue.set(state.allProducts, oldProductIndex, product)
            // state.allProducts[oldProductIndex] = product
        },

        deleteProductByID(state, product) {
            const oldProduct = state.allProducts.find(({ id }) => product.id == id)
            const oldProductIndex = state.allProducts.indexOf(oldProduct)
            state.allProducts.splice(oldProductIndex, 1)
        },

        LOADING(state, bool) {
            state.loading = bool
            console.log(state.loading)
        }
    },

    actions: {
        loadProducts(store) {
            axios
                .get("/api/v1/products")
                .then(r => r.data)
                .then(allProducts => {
                    store.commit('SET_PRODUCTS', allProducts)

                })
        },

        addProduct(store, product) {
            store.commit('LOADING', true)
            axios
                .put("/api/v1/products/" + product.id, {
                    available: product.available + 1,
                }).then(response => {
                    const product = response.data
                    store.commit('replaceProductByID', product)
                    store.commit('LOADING', false)
                })
        },

        removeProduct(store, product) {
            store.commit('LOADING', true)
            axios
                .put("/api/v1/products/" + product.id, {
                    available: product.available - 1,
                }).then(response => {
                    const product = response.data
                    store.commit('replaceProductByID', product)
                    store.commit('LOADING', false)
                })
        },
        //I want to add a loading circle here, because now it does delete the item, but it takes a while, and also it doesn't refresh the page
        deleteProduct(store, product) {
            store.commit('LOADING', true)
            axios
                .delete("/api/v1/products/" + product.id)
                .then(function (response) {
                    Vue.notify({
                        group: 'notif',
                        title: product.name,
                        text: 'Product verwijderd'
                    })
                    store.commit('deleteProductByID', product)
                    store.commit('LOADING', false)
                })
        },

        addNewProduct(store, { name, photo }) {
            store.commit('LOADING', true)
            axios
                .post("/api/v1/products/", {
                    name: name,
                    photo: photo,
                    available: 0,
                })
                .then(function (response) {
                    Vue.notify({
                        group: 'notif',
                        title: name,
                        text: 'Product toegevoegd'
                    })
                    store.commit('LOADING', false)
                    location.replace("/#/overview");
                })
        },

        editProduct(store, product) {
            axios
                .put("/api/v1/products/" + product.id, {
                    name: product.name,
                    photo: product.photo
                })
                .then(function (response) {
                    Vue.notify({
                        group: 'notif',
                        title: product.name,
                        text: 'Product aangepast'
                    })
                    location.replace("/#/overview");
                })
        },

        addtoCart(store, product) {

            store.commit('addtoCart', product)
        }
    }
});
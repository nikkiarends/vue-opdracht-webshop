import Vue from 'vue'
import axios from 'axios'
import Notifications from 'vue-notification'
import App from './App.vue'
import Router from 'vue-router'
import store from './store/store'
import ProductOverview from './Components/ProductOverview.vue'
import NewProduct from './Components/NewProduct.vue'
import Home from './Home.vue'
import ModernView from './Components/ModernView.vue'
import ProductShow from './Components/ProductShow.vue'
import EditProduct from './Components/EditProduct.vue'
import Cart from './Components/Cart.vue'


Vue.use(Router)
Vue.use(Notifications)
Vue.prototype.$axios = axios


const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/newproduct',
      name: 'newproduct',
      component: NewProduct,
    },
    {
      path: '/overview',
      name: 'overview',
      component: ProductOverview,
    },

    {
      path: '/modernview',
      name: 'modernview',
      component: ModernView,
    },
    {
      path: '/product/:id',
      name: 'product.show',
      component: ProductShow,

    },
    {
      path: '/product/:id/edit',
      name: 'product.edit',
      component: EditProduct,
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart,
    },
  ]
})

new Vue({
  el: '#app',
  store,
  render: h => h(App),
  router
})


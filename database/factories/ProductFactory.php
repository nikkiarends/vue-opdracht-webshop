<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'available' => rand(1, 15),
        'photo' => 'https://screenshotlayer.com/images/assets/placeholder.png',
    ];
});
